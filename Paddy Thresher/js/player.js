var source = document.getElementById("audio");
var lang = document.getElementById("language");
// 2. This code loads the IFrame Player API code asynchronously.
var tag = document.createElement("script");

tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName("script")[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

// 3. This function creates an <iframe> (and YouTube player)
//    after the API code downloads.
var player;
function onYouTubeIframeAPIReady() {
  player = new YT.Player("player", {
    height: "390",
    width: "640",
    videoId: videoID,
    playerVars: {
      controls: 0,
      disablekb: 1,
      mute: 0,
      rel: 0
    },
    events: {
      onReady: onPlayerReady,
      onStateChange: onPlayerStateChange,
      custom: customEvent
    },
  });
}

/*customEvent mutes player if language is english. For other language it plays the language audio*/
function customEvent(event) {
  if(typeof audio_lang !== "undefined"){
    source.setAttribute("src", audio_lang);
    event.target.mute();
  }
  else{
    event.target.unMute();
  }
}

function onPlayerReady(event) {
  if(typeof audio_lang !== "undefined"){
    source.setAttribute("src", audio_lang);
    event.target.mute();
    console.log("undefined");
  }
  else{
    event.target.unMute();
  }
}

// 5. The API calls this function when the player's state changes.
//    The function indicates that when playing a video (state=1),

function onPlayerStateChange(event) {
  // console.log("YT.PlayerState.PLAYING: ",YT.PlayerState.PLAYING);
  // console.log("YT.PlayerState.PLAYING: ", event.data);
  if (event.data == YT.PlayerState.PLAYING) {
    source.play();
  }
  switch (event.data) {
    case 1:
      source.play();
      break;
    case 2:
      source.pause();
      break;
    case 3:
        source.pause();
        break;
  }

}

function stopVideo() {
  player.stopVideo();
  source.pause();
}

function changeLanguage() {
  changedVal = lang.value;
  switch (changedVal) {
    case "English":
      source.setAttribute("src", "");
      player.seekTo(0, true);
      player.unMute();
      alert("You are about to switch to English");
      break;
    case "தமிழ்":
      source.pause();
      source.setAttribute("src", "audio/tamil.mp3");
      source.play();
      player.seekTo(0, true);
      player.mute();
      alert("You are about to switch to Tamil");
      break;
      case "हिन्दी":
        source.setAttribute("src", "audio/hindi.mp3");
        source.play();
        player.seekTo(0, true);
        player.mute();
        alert("You are about to switch to Hindi");
        break;
    default:
      break;
  }
}
